package com.ncs.ncsgroup.service;

import org.springframework.stereotype.Service;

import com.ncs.ncsgroup.entity.SOC;
import com.ncs.ncsgroup.security.UserPrincipal;

@Service
public interface UserService {

    SOC createUser(SOC user);

    UserPrincipal findByUsername(String username);

    UserPrincipal findById(Integer userId);

}
