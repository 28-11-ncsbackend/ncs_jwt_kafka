package com.ncs.ncsgroup.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.ncs.ncsgroup.entity.History;
import com.ncs.ncsgroup.repository.HistoryRepository;

import javax.transaction.Transactional;

@Service
public class HistoryService {
    
    @Autowired
    HistoryRepository historyRepository;

    public List<History> RateLimit(Integer orgId,Integer socId){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Long now = timestamp.getTime();
        Long beforeFiveMinutes = now - 300000;
        List<History> findHistory = historyRepository.findByRateLimit(beforeFiveMinutes,now,orgId,socId);
        return findHistory;
    }

//    @Scheduled(cron = "0 0 0 * * *")
    @Scheduled(fixedDelay = 3600000)
    @Transactional
    public void clearHistoryTableEveryDay() throws InterruptedException {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Long now = timestamp.getTime();
        historyRepository.clearHistoryEveryDay(now - 86400000);
        System.out.println("Auto clear history table everyday!");
    }

}
