package com.ncs.ncsgroup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ncs.ncsgroup.entity.SOC;
import com.ncs.ncsgroup.repository.SOCRepository;
import com.ncs.ncsgroup.security.UserPrincipal;
import com.ncs.ncsgroup.service.UserService;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private SOCRepository userRepository;

    @Override
    public SOC createUser(SOC user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public UserPrincipal findById(Integer socId){
        Optional<SOC> user = userRepository.findById(socId);
        SOC userData = user.get();
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != user) {
            Set<String> authorities = new HashSet<>();
            if (null != userData.getOrgs())
            userData.getOrgs().forEach(r -> {
                    authorities.add(String.valueOf(r.getOrg_id()));
                });
            userPrincipal.setUserId(userData.getSoc_id());
            userPrincipal.setUsername(userData.getSoc_name());
            userPrincipal.setAuthorities(authorities);
            userPrincipal.setRestrictInterval(userData.getRestrict_interval());
        }
        return userPrincipal;
    }

    @Override
    public UserPrincipal findByUsername(String socName) {
        SOC user = userRepository.findBySOCName(socName);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != user) {
            Set<String> authorities = new HashSet<>();
            if (null != user.getOrgs())
                user.getOrgs().forEach(r -> {
                    authorities.add(String.valueOf(r.getOrg_id()));
                });
            userPrincipal.setUserId(user.getSoc_id());
            userPrincipal.setUsername(user.getSoc_name());
            userPrincipal.setAuthorities(authorities);
            userPrincipal.setRestrictInterval(user.getRestrict_interval());
        }
        return userPrincipal;
    }

}
