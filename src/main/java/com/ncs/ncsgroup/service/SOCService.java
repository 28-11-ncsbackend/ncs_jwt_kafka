package com.ncs.ncsgroup.service;

import com.ncs.ncsgroup.entity.SOC;
import com.ncs.ncsgroup.repository.SOCRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SOCService {

    @Autowired
    SOCRepository socRepository;

    public SOC findSocByUsernameFromHeader(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String socName = authentication.getName();
        SOC soc = socRepository.findBySOCName(socName);
        return soc;
    }

    public SOC getSocByName(String socName){
        SOC soc = socRepository.findBySOCName(socName);
        return soc;
    }

}
