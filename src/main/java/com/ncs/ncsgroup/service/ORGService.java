package com.ncs.ncsgroup.service;

import com.ncs.ncsgroup.entity.ORG;
import com.ncs.ncsgroup.repository.ORGRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ORGService {

    @Autowired
    ORGRepository orgRepository;

    public ORG getORGByName(String orgName){
        ORG org = orgRepository.findByOrgName(orgName);
        return org;
    }

}
