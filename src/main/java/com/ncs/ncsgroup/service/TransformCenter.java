package com.ncs.ncsgroup.service;

import java.util.ArrayList;
import java.util.List;
import com.ncs.ncsgroup.entity.Body;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import com.ncs.ncsgroup.config.NcsConfig;
import com.ncs.ncsgroup.entity.Indicator;
import com.ncs.ncsgroup.transform.Artifact;
import com.ncs.ncsgroup.transform.AutonomousSystem;
import com.ncs.ncsgroup.transform.Directory;
import com.ncs.ncsgroup.transform.DomainName;
import com.ncs.ncsgroup.transform.Email;
import com.ncs.ncsgroup.transform.File;
import com.ncs.ncsgroup.transform.IpAddr;
import com.ncs.ncsgroup.transform.MacAddr;
import com.ncs.ncsgroup.transform.Mutex;
import com.ncs.ncsgroup.transform.Process;
import com.ncs.ncsgroup.transform.Software;
import com.ncs.ncsgroup.transform.Url;
import com.ncs.ncsgroup.transform.UserAccount;
import com.ncs.ncsgroup.transform.WindowsRegistryKey;

@Service
public class TransformCenter {
    
    @Autowired
    KafkaTemplate<String, Object> template;

    @Autowired
    BodyService bodyService;

    @Autowired
    NcsConfig ncsConfig;

    public void TransformCenter(List<Indicator> indicators, Body body,Integer socId,String socName,String orgName) {
        String debug = ncsConfig.getDebug();
        for (Indicator indicator : indicators) {
            String typeName = indicator.getType().toLowerCase();
            if (typeName.equals("artifact")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                Artifact artifact = new Artifact();
                artifact.setType(indicator.getType());
                artifact.setArtifact(indicator.getValue());
                artifact.setArtifactHash(indicator.getHash());
                formatBody.getIndicator().add(artifact);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("autonomous-system")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                AutonomousSystem autonomousSystem = new AutonomousSystem();
                autonomousSystem.setType(indicator.getType());
                autonomousSystem.setAsn(indicator.getValue());
                formatBody.getIndicator().add(autonomousSystem);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("directory")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                Directory directory = new Directory();
                directory.setType(indicator.getType());
                directory.setDirectory(indicator.getValue());
                formatBody.getIndicator().add(directory);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("domain-name")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                DomainName domainName = new DomainName();
                domainName.setType(indicator.getType());
                domainName.setDomainName(indicator.getValue());
                formatBody.getIndicator().add(domainName);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("email")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                Email email = new Email();
                email.setType(indicator.getType());
                email.setEmailAdress(indicator.getEmailAdress());
                email.setEmailSubject(indicator.getSubject());
                email.setEmailBody(indicator.getMessage());
                formatBody.getIndicator().add(email);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("file")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                File file = new File();
                file.setType(indicator.getType());
                file.setFileName(indicator.getFileName());
                file.setFileHash(indicator.getHash());
                formatBody.getIndicator().add(file);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("ip-addr")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                IpAddr ipAddr = new IpAddr();
                ipAddr.setType(indicator.getType());
                ipAddr.setIpv4Address(indicator.getIpv4Addr());
                ipAddr.setIpv6Address(indicator.getIpv6Addr());
                formatBody.getIndicator().add(ipAddr);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("mac-addr")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                MacAddr macAddr = new MacAddr();
                macAddr.setType(indicator.getType());
                macAddr.setMacAddress(indicator.getValue());
                formatBody.getIndicator().add(macAddr);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("mutex")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                Mutex mutex = new Mutex();
                mutex.setType(indicator.getType());
                mutex.setMutex(indicator.getValue());
                formatBody.getIndicator().add(mutex);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("process")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                Process process = new Process();
                process.setType(indicator.getType());
                process.setProcessName(indicator.getName());
                process.setProcessCwd(indicator.getCwd());
                process.setProcessCommandLine(indicator.getCommandLine());
                process.setProcessService(indicator.getService());
                formatBody.getIndicator().add(process);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("software")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                Software software = new Software();
                software.setType(indicator.getType());
                software.setSoftwareName(indicator.getName());
                software.setSoftwareVersion(indicator.getVersion());
                software.setSoftwareVendor(indicator.getVendor());
                software.setSoftwareCpe(indicator.getCpe());
                formatBody.getIndicator().add(software);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("url")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                Url url = new Url();
                url.setType(indicator.getType());
                url.setUrl(indicator.getValue());
                formatBody.getIndicator().add(url);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("user-account")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                UserAccount userAccount = new UserAccount();
                userAccount.setType(indicator.getType());
                userAccount.setAccountLogin(indicator.getAccountLogin());
                userAccount.setIsServiceAccount(indicator.getIsServiceAccount());
                userAccount.setIsPrivileged(indicator.getIsPrivileged());
                userAccount.setCanEscaltePrivs(indicator.getCanEscaltePrivs());
                formatBody.getIndicator().add(userAccount);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
            if (typeName.equals("registry")) {
                Body formatBody = bodyService.formatBody(body,socId,socName,orgName);
                WindowsRegistryKey windowsRegistryKey = new WindowsRegistryKey();
                windowsRegistryKey.setType(indicator.getType());
                windowsRegistryKey.setRegistryValue(indicator.getValue());
                windowsRegistryKey.setRegistryKey(indicator.getKey());
                formatBody.getIndicator().add(windowsRegistryKey);
                 template.send(ncsConfig.getKafka().getTopicname(), formatBody);
                if (debug.equals("true")) {
                    System.out.println(formatBody.toString());
                }
            }
        }
    }
}
