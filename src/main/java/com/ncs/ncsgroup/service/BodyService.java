package com.ncs.ncsgroup.service;

import com.ncs.ncsgroup.entity.Body;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class BodyService {

    public Body formatBody(Body body,Integer socId,String socName,String orgName){
        Body formatBody = new Body();

        formatBody.setOrgName(orgName);
        formatBody.setSocName(socName);
        formatBody.setSocId(socId);
        formatBody.setLocal_incident_id(body.getLocal_incident_id());
        formatBody.setOrg_id(body.getOrg_id());
        formatBody.setSystem_impact(body.getSystem_impact());
        formatBody.setCritical_level(body.getCritical_level());
        formatBody.setNation_critical_infrastructure(body.getNation_critical_infrastructure());
        formatBody.setIncident_name(body.getIncident_name());
        formatBody.setResolution(body.getResolution());
        formatBody.setStatus(body.getStatus());
        formatBody.setSeverity(body.getSeverity());
        formatBody.setAttack_vector(body.getAttack_vector());
        formatBody.setDescription(body.getDescription());
        formatBody.setAttack_time(body.getAttack_time());
        formatBody.setDetected_time(body.getDetected_time());
        formatBody.setIsolated_time(body.getIsolated_time());
        formatBody.setClosed_time(body.getClosed_time());
        formatBody.setReported_time(body.getReported_time());
        formatBody.setIndicator(new ArrayList<>());

        return formatBody;
    }
}
