package com.ncs.ncsgroup.service;

import org.springframework.stereotype.Service;
import com.ncs.ncsgroup.entity.Token;

@Service
public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);

}
