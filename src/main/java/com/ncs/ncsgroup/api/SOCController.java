package com.ncs.ncsgroup.api;

import java.util.*;
import javax.validation.Valid;
import com.ncs.ncsgroup.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import com.ncs.ncsgroup.entity.Body;
import com.ncs.ncsgroup.entity.History;
import com.ncs.ncsgroup.entity.Indicator;
import com.ncs.ncsgroup.entity.ORG;
import com.ncs.ncsgroup.entity.SOC;
import com.ncs.ncsgroup.entity.Token;
import com.ncs.ncsgroup.repository.HistoryRepository;
import com.ncs.ncsgroup.repository.ORGRepository;
import com.ncs.ncsgroup.repository.SOCRepository;
import com.ncs.ncsgroup.security.JwtUtil;
import com.ncs.ncsgroup.security.UserPrincipal;
import java.sql.Timestamp;

@RestController
@CrossOrigin
@RequestMapping("/soc")
public class SOCController {

    @Autowired
    private UserService userService;

    @Autowired
    ORGService orgService;

    @Autowired
    private TransformCenter transformCenter;

    @Autowired
    private TokenService tokenService;

    @Autowired
    SOCRepository socRepository;

    @Autowired
    BodyService bodyService;

    @Autowired
    ORGRepository orgRepository;

    @Autowired
    SOCService socService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    HistoryService historyService;

    @Autowired
    ResponseService responseService;

    @PostMapping("/create-soc")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> register(@Valid @RequestBody SOC soc) {
        try {
                SOC checkSoc = socService.getSocByName(soc.getSoc_name());
                if(checkSoc != null){
                    Map<String, Object> uniqueSocNameBody = responseService.responseBody(400,"Security operation center name already exists.",null);
                    return new ResponseEntity<>(uniqueSocNameBody,HttpStatus.BAD_REQUEST);
                } else {
                    SOC newSOC = new SOC();
                    newSOC.setSoc_name(soc.getSoc_name());
                    Set<ORG> orgs = soc.getOrgs();
                    Set<ORG> setSocOrgs = new HashSet<>();
                    for (ORG org : orgs){
                        ORG checkUniqueOrgName = orgService.getORGByName(org.getOrg_name());
                        if(checkUniqueOrgName != null){
                            Map<String, Object> uniqueOrgBody = responseService.responseBody(400,org.getOrg_name() + " is already exists.",null);
                            return new ResponseEntity<>(uniqueOrgBody,HttpStatus.BAD_REQUEST);
                        } else {
                            setSocOrgs.add(org);
                        }
                    };
                    newSOC.setOrgs(setSocOrgs);
                    newSOC.setRestrict_interval(soc.getRestrict_interval());
                    SOC saveSOC = socRepository.save(newSOC);
                    Token token = new Token();
                    token.setToken(jwtUtil.generateToken(saveSOC.getSoc_id()));
                    token.setTokenExpDate(jwtUtil.generateExpirationDate());
                    tokenService.createToken(token);
                    saveSOC.setToken(token.getToken());
                    SOC saveSOC2 = socRepository.save(newSOC);
                    return new ResponseEntity<SOC>(saveSOC2, HttpStatus.CREATED);
                }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/get-soc/get-by-name/{socName}")
    public ResponseEntity<?> getbyName(@PathVariable("socName") String socName) {
        try {
            SOC soc = socRepository.findBySOCName(socName);
            if(soc != null){
                return new ResponseEntity<>(soc, HttpStatus.OK);
            } else {
                Map<String,Object> bodyFindFailed = responseService.responseBody(404,"SOC does not exists.",null);
                return new ResponseEntity<>(bodyFindFailed,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/get-soc/get-by-id/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Integer socId) {
        try {
            Optional<SOC> findSOC = socRepository.findById(socId);
            if (findSOC.isPresent()) {
                SOC socData = findSOC.get();
                return new ResponseEntity<>(socData, HttpStatus.OK);
            } else {
                Map<String,Object> bodyFindFailed = responseService.responseBody(404,"SOC does not exists.",null);
                return new ResponseEntity<>(bodyFindFailed, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/get-soc/get-all")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<SOC>> getAllSOC() {
        try {
            List<SOC> socs = new ArrayList<>();
            socRepository.findAll().forEach(socs::add);
            return new ResponseEntity<>(socs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/delete-soc/delete-by-id/{id}")
    public ResponseEntity<?> deteleSocById(@PathVariable("id") Integer socId) {
        try {
            socRepository.deleteById(socId);
            Map<String,Object> bodyDeleteSuccess = responseService.responseBody(204,"Delete SOC successful.",null);
            return new ResponseEntity<>(bodyDeleteSuccess, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/post-event")
    @PreAuthorize("hasAuthority(#pBody.getOrg_id())")
    public ResponseEntity<?> postEvent(@Valid @RequestBody Body pBody) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String socName = authentication.getName();
            UserPrincipal findUser = userService.findByUsername(socName);
            Optional<ORG> findOrg = orgRepository.findById(pBody.getOrg_id());
            ORG orgData = findOrg.get();
            Integer socId = findUser.getUserId();
            Integer restrictInterval = findUser.getRestrictInterval();
            History history = new History();
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            if(restrictInterval != 0){
                List<History> list = historyService.RateLimit(pBody.getOrg_id(), socId);
                if (list.size() >= restrictInterval) {
                    Map<String, Object> restrictIntervalBody = responseService.responseBody(418,"Rate Litmited.",null);
                    return new ResponseEntity<>(restrictIntervalBody, HttpStatus.I_AM_A_TEAPOT);
                } 
            }
            List<Indicator> indicators = pBody.getIndicator();
            if(indicators != null){
                transformCenter.TransformCenter(indicators,pBody,socId,socName,orgData.getOrg_name());
            }
            history.setOrg_id(pBody.getOrg_id());
            history.setSoc_id(socId);
            history.setLast_update(timestamp.getTime());
            historyRepository.save(history);
            Map<String, Object> bodySuccess = responseService.responseBody(200,"Send kafka successful.",history);
            return new ResponseEntity<>(bodySuccess, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add-org/{socId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> addOrg(@Valid @RequestBody ORG pORG,@PathVariable("socId") Integer socId) {
        try {
            Optional<SOC> findSOC = socRepository.findById(socId);
            if(findSOC.isPresent()){
                SOC findSocData = findSOC.get();
                ORG findOrg = orgRepository.findByOrgName(pORG.getOrg_name());
                if (findOrg == null) {
                    ORG newOrg = new ORG();
                    newOrg.setOrg_name(pORG.getOrg_name());
                    orgRepository.save(newOrg);
                    findSocData.getOrgs().add(newOrg);
                    SOC saveSOC = socRepository.save(findSocData);
                    Map<String,Object> bodySuccess = responseService.responseBody(201,"Update SOC successful with a new ORG.",saveSOC);
                    return new ResponseEntity<>(bodySuccess, HttpStatus.OK);
                } else {
                    findSocData.getOrgs().add(findOrg);
                    SOC saveSOC = socRepository.save(findSocData);
                    Map<String,Object> bodySuccess = responseService.responseBody(201,"Update SOC successful.",saveSOC);
                    return new ResponseEntity<>(bodySuccess, HttpStatus.OK);
                }
            } else {
                Map<String,Object> bodyFailed = responseService.responseBody(404,"SOC does not exists.",null);
                return new ResponseEntity<>(bodyFailed,HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/remove-org/{socId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> removeOrg(@Valid @RequestBody ORG pORG,@PathVariable("socId") Integer socId) {
        try {
            ORG findOrg = orgRepository.findByOrgName(pORG.getOrg_name());
            Optional<SOC> findSoc = socRepository.findById(socId);
            if(findSoc.isPresent()) {
                SOC findSocData = findSoc.get();
                Set<ORG> orgs = findSocData.getOrgs();
                Boolean checkOrgExists = false;
                for (ORG org: orgs){
                    if(org.getOrg_name().equals(pORG.getOrg_name())){
                        checkOrgExists = true;
                    }
                }
                if(checkOrgExists == true){
                    findSocData.getOrgs().remove(findOrg);
                    SOC saveSOC = socRepository.save(findSocData);
                    Map<String,Object> body = responseService.responseBody(200,"Remove ORG successful.",saveSOC);
                    return new ResponseEntity<>(body, HttpStatus.OK);
                } else {
                    Map<String,Object> body = responseService.responseBody(404,"ORG does not exists.",null);
                    return new ResponseEntity<>(body,HttpStatus.NOT_FOUND);
                }
            } else {
                Map<String,Object> bodyFailed = responseService.responseBody(404,"SOC does not exists.",null);
                return new ResponseEntity<>(bodyFailed,HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update-soc-info/{socId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> updateSoc(@RequestBody SOC pSoc,@PathVariable("socId") Integer socId){
        try {
            Optional<SOC> findSoc = socRepository.findById(socId);
            if(findSoc.isPresent()){
                SOC findSocData = findSoc.get();
                SOC checkSoc = socService.getSocByName(pSoc.getSoc_name());
                if(checkSoc != null && checkSoc.getSoc_id() != findSocData.getSoc_id()){
                    Map<String,Object> bodyFailed = responseService.responseBody(400,"Security operation center name is already exists.",null);
                    return new ResponseEntity<>(bodyFailed,HttpStatus.BAD_REQUEST);
                }
                if(checkSoc != null && checkSoc.getSoc_id() == findSocData.getSoc_id()){
                    checkSoc.setSoc_name(pSoc.getSoc_name());
                    checkSoc.setRestrict_interval(pSoc.getRestrict_interval());
                    SOC saveSoc = socRepository.save(checkSoc);
                    Map<String,Object> bodySuccess = responseService.responseBody(200,"Update information successful.",saveSoc);
                    return new ResponseEntity<>(bodySuccess,HttpStatus.OK);
                }
                else {
                    findSocData.setSoc_name(pSoc.getSoc_name());
                    findSocData.setRestrict_interval(pSoc.getRestrict_interval());
                    SOC saveSoc = socRepository.save(findSocData);
                    Map<String,Object> bodySuccess = responseService.responseBody(200,"Update information successful.",saveSoc);
                    return new ResponseEntity<>(bodySuccess,HttpStatus.OK);
                }
            } else {
                Map<String,Object> bodyFailed = responseService.responseBody(404,"SOC does not exists.",null);
                return new ResponseEntity<>(bodyFailed,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
