package com.ncs.ncsgroup.api;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ncs.ncsgroup.entity.ORG;
import com.ncs.ncsgroup.repository.ORGRepository;

@RestController
@RequestMapping("/org")
@CrossOrigin
public class ORGController {
    
    @Autowired
    ORGRepository orgRepository;

    @PostMapping("/create-org")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ORG> createORG(@Valid @RequestBody ORG pOrg){
        try {
            ORG newOrg = new ORG();
            newOrg.setOrg_name(pOrg.getOrg_name());
            ORG saveOrg = orgRepository.save(newOrg);
            return new ResponseEntity<>(saveOrg,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-org/get-by-orgname/{orgName}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ORG> getOrgByName(@PathVariable("orgName") String orgName){
        try {
            ORG org = orgRepository.findByOrgName(orgName);
            if(org != null){
                return new ResponseEntity<>(org,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-org/get-by-id/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ORG> getOrgById(@PathVariable("id") Integer orgId){
        try {
            Optional<ORG> findOrg = orgRepository.findById(orgId);
            if(findOrg.isPresent()){
                ORG orgData = findOrg.get();
                return new ResponseEntity<>(orgData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-org/get-all")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<ORG>> getAllOrg(){
        try {
            List<ORG> orgs = new ArrayList<>();
            orgRepository.findAll().forEach(orgs::add);
            return new ResponseEntity<>(orgs,HttpStatus.OK);
        } catch(Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update-org/update-by-id/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ORG> updateOrg(@PathVariable("id") Integer orgId,@Valid @RequestBody ORG pORG){
        try {
            Optional<ORG> findOrg = orgRepository.findById(orgId);
            if(findOrg.isPresent()){
                ORG orgData = findOrg.get();
                orgData.setOrg_name(pORG.getOrg_name());
                ORG saveOrg = orgRepository.save(orgData);
                return new ResponseEntity<>(saveOrg,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete-org/delete-by-id/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteOrg(@PathVariable("id") Integer orgId){
        try {
            Optional<ORG> findOrg = orgRepository.findById(orgId);
            if(findOrg.isPresent()){
                orgRepository.deleteById(orgId);
                return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
