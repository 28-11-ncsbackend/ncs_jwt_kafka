package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class UserAccount extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Account Login")
    private String accountLogin;

    @JsonProperty("Is Service Account")
    private String isServiceAccount;

    @JsonProperty("Is Privileged")
    private String isPrivileged;

    @JsonProperty("Can Escalate Privs")
    private String canEscaltePrivs;

    public UserAccount(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccountLogin() {
        return accountLogin;
    }

    public void setAccountLogin(String accountLogin) {
        this.accountLogin = accountLogin;
    }

    public String getIsServiceAccount() {
        return isServiceAccount;
    }

    public void setIsServiceAccount(String isServiceAccount) {
        this.isServiceAccount = isServiceAccount;
    }

    public String getIsPrivileged() {
        return isPrivileged;
    }

    public void setIsPrivileged(String isPrivileged) {
        this.isPrivileged = isPrivileged;
    }

    public String getCanEscaltePrivs() {
        return canEscaltePrivs;
    }

    public void setCanEscaltePrivs(String canEscaltePrivs) {
        this.canEscaltePrivs = canEscaltePrivs;
    }

    @Override
    public String toString() {
        return "UserAccount [type=" + type + ", accountLogin=" + accountLogin + ", isServiceAccount=" + isServiceAccount
                + ", isPrivileged=" + isPrivileged + ", canEscaltePrivs=" + canEscaltePrivs + "]";
    }


}
