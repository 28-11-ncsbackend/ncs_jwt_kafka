package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class File extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("File Name")
    private String fileName;

    @JsonProperty("File Hash")
    private String fileHash;

    public File(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    @Override
    public String toString() {
        return "File [type=" + type + ", fileName=" + fileName + ", fileHash=" + fileHash + "]";
    }

    

}
