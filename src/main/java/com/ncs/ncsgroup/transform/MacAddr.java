package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class MacAddr extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("MAC Address")
    private String macAddress;

    public MacAddr(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @Override
    public String toString() {
        return "MacAddr [type=" + type + ", macAddress=" + macAddress + "]";
    }

    
}
