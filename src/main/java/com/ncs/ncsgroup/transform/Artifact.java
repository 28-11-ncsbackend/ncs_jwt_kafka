package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class Artifact extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Artifact")

    private String artifact;

    @JsonProperty("Artifact Hash")
    private String artifactHash;

    public Artifact(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getArtifact() {
        return artifact;
    }

    public void setArtifact(String artifact) {
        this.artifact = artifact;
    }

    public String getArtifactHash() {
        return artifactHash;
    }

    public void setArtifactHash(String artifactHash) {
        this.artifactHash = artifactHash;
    }

    @Override
    public String toString() {
        return "Artifact [type=" + type + ", artifact=" + artifact + ", artifactHash=" + artifactHash + "]";
    }

    
}
