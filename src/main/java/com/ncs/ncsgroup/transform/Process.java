package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class Process extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Process Name")
    private String processName;

    @JsonProperty("Process CWD")
    private String processCwd;

    @JsonProperty("Process Command Line")
    private String processCommandLine;

    @JsonProperty("Process Service")
    private String processService;

    public Process(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessCwd() {
        return processCwd;
    }

    public void setProcessCwd(String processCwd) {
        this.processCwd = processCwd;
    }

    public String getProcessCommandLine() {
        return processCommandLine;
    }

    public void setProcessCommandLine(String processCommandLine) {
        this.processCommandLine = processCommandLine;
    }

    public String getProcessService() {
        return processService;
    }

    public void setProcessService(String processService) {
        this.processService = processService;
    }

    @Override
    public String toString() {
        return "Process [type=" + type + ", processName=" + processName + ", processCwd=" + processCwd
                + ", processCommandLine=" + processCommandLine + ", processService=" + processService + "]";
    }


    
}
