package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class WindowsRegistryKey extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Registry Key")
    private String registryKey;

    @JsonProperty("Registry Value")
    private String registryValue;

    public WindowsRegistryKey(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegistryKey() {
        return registryKey;
    }

    public void setRegistryKey(String registryKey) {
        this.registryKey = registryKey;
    }

    public String getRegistryValue() {
        return registryValue;
    }

    public void setRegistryValue(String registryValue) {
        this.registryValue = registryValue;
    }

    @Override
    public String toString() {
        return "WindowsRegistryKey [type=" + type + ", registryKey=" + registryKey + ", registryValue=" + registryValue
                + "]";
    }

    
}
