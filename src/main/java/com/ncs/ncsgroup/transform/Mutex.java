package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class Mutex extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("MutEx")
    private String mutex;

    public Mutex(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMutex() {
        return mutex;
    }

    public void setMutex(String mutex) {
        this.mutex = mutex;
    }

    @Override
    public String toString() {
        return "Mutex [type=" + type + ", mutex=" + mutex + "]";
    }

    
}
