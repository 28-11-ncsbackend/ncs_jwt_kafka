package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class IpAddr extends Indicator {
 
    @JsonProperty("type")
    private String type;

    @JsonProperty("IPv4 Address")
    private String ipv4Address;

    @JsonProperty("IPv6 Address")
    private String ipv6Address;

    public IpAddr(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIpv4Address() {
        return ipv4Address;
    }

    public void setIpv4Address(String ipv4Address) {
        this.ipv4Address = ipv4Address;
    }

    public String getIpv6Address() {
        return ipv6Address;
    }

    public void setIpv6Address(String ipv6Address) {
        this.ipv6Address = ipv6Address;
    }

    @Override
    public String toString() {
        return "IpAddr [type=" + type + ", ipv4Address=" + ipv4Address + ", ipv6Address=" + ipv6Address + "]";
    }

    
}
