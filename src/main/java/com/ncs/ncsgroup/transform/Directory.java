package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class Directory extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Directory")
    private String directory;

    public Directory(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    @Override
    public String toString() {
        return "Directory [type=" + type + ", directory=" + directory + "]";
    }

    
}
