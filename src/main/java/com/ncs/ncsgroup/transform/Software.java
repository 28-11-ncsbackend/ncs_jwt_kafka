package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class Software extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Software Name")
    private String softwareName;

    @JsonProperty("Software Version")
    private String softwareVersion;

    @JsonProperty("Software Vendor")
    private String softwareVendor;

    @JsonProperty("Software CPE")
    private String softwareCpe;

    public Software(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSoftwareName() {
        return softwareName;
    }

    public void setSoftwareName(String softwareName) {
        this.softwareName = softwareName;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public String getSoftwareVendor() {
        return softwareVendor;
    }

    public void setSoftwareVendor(String softwareVendor) {
        this.softwareVendor = softwareVendor;
    }

    public String getSoftwareCpe() {
        return softwareCpe;
    }

    public void setSoftwareCpe(String softwareCpe) {
        this.softwareCpe = softwareCpe;
    }

    @Override
    public String toString() {
        return "Software [type=" + type + ", softwareName=" + softwareName + ", softwareVersion=" + softwareVersion
                + ", softwareVendor=" + softwareVendor + ", softwareCpe=" + softwareCpe + "]";
    }
    
    
}
