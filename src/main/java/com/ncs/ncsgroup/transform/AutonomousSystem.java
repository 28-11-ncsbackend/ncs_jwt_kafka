package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class AutonomousSystem extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("ASN")
    private String asn;

    public AutonomousSystem(){


    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAsn() {
        return asn;
    }

    public void setAsn(String asn) {
        this.asn = asn;
    }

    @Override
    public String toString() {
        return "AutonomousSystem [type=" + type + ", asn=" + asn + "]";
    }

    
}
