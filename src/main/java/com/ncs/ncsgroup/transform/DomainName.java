package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class DomainName extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Domain Name")
    private String domainName;

    public DomainName(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    @Override
    public String toString() {
        return "DomainName [type=" + type + ", domainName=" + domainName + "]";
    }
    
    
}
