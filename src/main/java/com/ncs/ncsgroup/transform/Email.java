package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class Email extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("Email Address")
    private String emailAdress;

    @JsonProperty("Email Subject")
    private String emailSubject;

    @JsonProperty("Email Body")
    private String emailBody;

    public Email(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    @Override
    public String toString() {
        return "Email [type=" + type + ", emailAdress=" + emailAdress + ", emailSubject=" + emailSubject
                + ", emailBody=" + emailBody + "]";
    }

    

}
