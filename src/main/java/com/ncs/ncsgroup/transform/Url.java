package com.ncs.ncsgroup.transform;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.entity.Indicator;

public class Url extends Indicator {
    
    @JsonProperty("type")
    private String type;

    @JsonProperty("URL")
    private String url;

    public Url(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Url [type=" + type + ", url=" + url + "]";
    }

    
}
