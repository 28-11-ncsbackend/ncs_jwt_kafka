package com.ncs.ncsgroup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ncs.ncsgroup.entity.ORG;

@Repository
public interface ORGRepository extends JpaRepository<ORG, Integer> {
    
    @Query("select o from ORG o where o.org_name = ?1")
    ORG findByOrgName(String orgName);

}
