package com.ncs.ncsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ncs.ncsgroup.entity.History;

@Repository
public interface HistoryRepository extends JpaRepository<History,Integer> {
    
    @Query(value = "SELECT * FROM update_history_table WHERE `org-id` LIKE :orgId% AND `soc-id` LIKE :socId%",nativeQuery = true)
    History findByOrgIdAndSocId(@Param("orgId") Integer org_id,@Param("socId") Integer soc_id);
    
    @Query(value = "SELECT h FROM History h WHERE ?1 <= h.last_update AND h.last_update <=  ?2 AND h.org_id = ?3 AND h.soc_id = ?4")
    List<History> findByRateLimit(Long beforeFiveMinutes,Long now,Integer orgId,Integer socId);

    @Modifying
    @Query(value = "DELETE FROM History h WHERE h.last_update <= ?1")
    void clearHistoryEveryDay(Long now);
}
