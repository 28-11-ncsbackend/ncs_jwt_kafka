package com.ncs.ncsgroup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ncs.ncsgroup.entity.SOC;

@Repository
public interface SOCRepository extends JpaRepository<SOC, Integer> {
    @Query("select s from SOC s where s.soc_name = ?1")
    SOC findBySOCName(String socName);
    
}
