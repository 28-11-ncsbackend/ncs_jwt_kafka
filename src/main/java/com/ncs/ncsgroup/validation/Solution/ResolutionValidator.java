package com.ncs.ncsgroup.validation.Solution;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class ResolutionValidator implements ConstraintValidator<Resolution,String> {
    List<String> resolution = Arrays.asList("false positive", "in progress/investigating", "containment achived","defered/delayed","escalated","restoration achived","not specified");
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(s != null){
            return resolution.contains(s.toLowerCase());
        } else {
            return false;
        }
    }
}