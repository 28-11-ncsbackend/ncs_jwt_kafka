package com.ncs.ncsgroup.validation.Severity;

import com.ncs.ncsgroup.validation.AttackVector.VectorAttack;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class SeverityValidator implements ConstraintValidator<Severity,String> {
    public static List<String> vector_attack = Arrays.asList("low","medium","high","critical");
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        String tmp = s;
        if(s != null){
            return vector_attack.contains(s.toLowerCase());
        } else {
            return false;
        }
    }
}