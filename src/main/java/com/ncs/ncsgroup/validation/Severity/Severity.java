package com.ncs.ncsgroup.validation.Severity;

import com.ncs.ncsgroup.validation.AttackVector.AttackVectorValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = SeverityValidator.class)
@Documented
public @interface Severity {
    String message() default "Severity is incorrect.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}