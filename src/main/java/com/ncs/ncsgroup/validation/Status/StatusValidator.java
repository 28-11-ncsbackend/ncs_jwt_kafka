package com.ncs.ncsgroup.validation.Status;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class StatusValidator implements ConstraintValidator<Status,String> {
    List<String> status = Arrays.asList("open","close");
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(s != null){
            return status.contains(s.toLowerCase());
        } else {
            return false;
        }

    }
}