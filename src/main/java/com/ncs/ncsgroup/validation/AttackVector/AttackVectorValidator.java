package com.ncs.ncsgroup.validation.AttackVector;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class AttackVectorValidator implements ConstraintValidator<VectorAttack,String> {
    public static List<String> vector_attack = Arrays.asList("attrition", "web", "email/phishing", "external/removable media", "impersonation/spoofing", "improper usage",
            "loss or theft of equipment", "vulnerability exploit", "unknown", "other"
            );
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        String tmp = s;
        if( s != null){
            return vector_attack.contains(s.toLowerCase());
        } else {
            return false;
        }
    }
}