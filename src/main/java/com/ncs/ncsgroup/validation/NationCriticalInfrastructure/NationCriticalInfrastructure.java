package com.ncs.ncsgroup.validation.NationCriticalInfrastructure;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = NationCriticalInfrastructureValidator.class)
@Documented
public @interface NationCriticalInfrastructure {
    String message() default "Nation critical infrastructure is incorrect.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}