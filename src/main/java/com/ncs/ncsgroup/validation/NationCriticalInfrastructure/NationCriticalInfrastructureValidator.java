package com.ncs.ncsgroup.validation.NationCriticalInfrastructure;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class NationCriticalInfrastructureValidator implements ConstraintValidator<NationCriticalInfrastructure,String> {
    List<String> nation_critical_infrastructure = Arrays.asList("yes","no");
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(s != null){
            return nation_critical_infrastructure.contains(s.toLowerCase());
        } else {
            return false;
        }
    }
}