package com.ncs.ncsgroup.entity;

import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ncs.ncsgroup.validation.AttackVector.VectorAttack;
import com.ncs.ncsgroup.validation.NationCriticalInfrastructure.NationCriticalInfrastructure;
import com.ncs.ncsgroup.validation.Severity.Severity;
import com.ncs.ncsgroup.validation.Solution.Resolution;
import com.ncs.ncsgroup.validation.Status.Status;

public class Body {
    @JsonProperty("local-incident-id")
    @NotNull(message = "Local incident must not be null.")
    private Integer local_incident_id;

    @JsonProperty("org-name")
    private String orgName;

    @JsonProperty("soc-id")
    private Integer socId;

    @JsonProperty("soc-name")
    private String socName;

    @JsonProperty("organization-id")
    @NotNull(message = "Organization id must not be null.")
    private Integer org_id;

    @JsonProperty("system-impact")
    private List<String> system_impact;

    @JsonProperty("critical-level")
    @NotNull(message = "Critical level must not be null.")
    @Min(value = 1,message = "Critical Level must be more than or equal to 1.")
    @Max(value = 4,message = "Critical level must be less than or equal to 4.")
    private Integer critical_level;

    @JsonProperty("nation-critical-infrastructure")
    @NotNull(message = "Nation critical infrastructure must not be null.")
    @NationCriticalInfrastructure
    private String nation_critical_infrastructure;

    @NotNull(message = "Severtiry must not be null.")
    @Severity
    private String severity;

    @JsonProperty("incident-name")
    @NotBlank(message = "Incident name must not be blank.")
    private String incident_name;

    @NotNull(message = "Resolution must not be null.")
    @Resolution
    private String resolution;
    @NotNull(message = "Status must not be null.")
    @Status
    private String status;

    @JsonProperty("attack-vector")
    @NotNull(message = "Attack vector must not be null.")
    @VectorAttack
    private String attack_vector;

    @NotBlank(message = "Description must not be blank.")
    private String description;

    @JsonProperty("attack-time")
    private Long attack_time;

    @NotNull(message = "Detected time must not be null.")
    @JsonProperty("detected-time")
    private Long detected_time;

    @JsonProperty("isolated-time")
    private Long isolated_time;

    @NotNull(message = "Close time must not be null.")
    @JsonProperty("closed-time")
    private Long closed_time;

    @NotNull(message = "Reported time must not be null.")
    @JsonProperty("reported-time")
    private Long reported_time;

    private List<Indicator> indicator;

    public Integer getLocal_incident_id() {
        return local_incident_id;
    }

    public void setLocal_incident_id(Integer local_incident_id) {
        this.local_incident_id = local_incident_id;
    }

    public Integer getOrg_id() {
        return org_id;
    }

    public void setOrg_id(Integer org_id) {
        this.org_id = org_id;
    }

    public Integer getCritical_level() {
        return critical_level;
    }

    public void setCritical_level(Integer critical_level) {
        this.critical_level = critical_level;
    }

    public String getIncident_name() {
        return incident_name;
    }


    public void setIncident_name(String incident_name) {
        this.incident_name = incident_name;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAttack_vector() {
        return attack_vector;
    }

    public void setAttack_vector(String attack_vector) {
        this.attack_vector = attack_vector;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAttack_time() {
        return attack_time;
    }

    public void setAttack_time(Long attack_time) {
        this.attack_time = attack_time;
    }

    public Long getDetected_time() {
        return detected_time;
    }

    public void setDetected_time(Long detected_time) {
        this.detected_time = detected_time;
    }

    public Long getIsolated_time() {
        return isolated_time;
    }

    public void setIsolated_time(Long isolated_time) {
        this.isolated_time = isolated_time;
    }

    public Long getReported_time() {
        return reported_time;
    }

    public void setReported_time(Long reported_time) {
        this.reported_time = reported_time;
    }

    public Long getClosed_time() {
        return closed_time;
    }

    public void setClosed_time(Long closed_time) {
        this.closed_time = closed_time;
    }

    public String getNation_critical_infrastructure() {
        return nation_critical_infrastructure;
    }

    public void setNation_critical_infrastructure(String nation_critical_infrastructure) {
        this.nation_critical_infrastructure = nation_critical_infrastructure;
    }

    public List<String> getSystem_impact() {
        return system_impact;
    }

    public void setSystem_impact(List<String> system_impact) {
        this.system_impact = system_impact;
    }

    public List<Indicator> getIndicator() {
        return indicator;
    }

    public void setIndicator(List<Indicator> indicator) {
        this.indicator = indicator;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    @Override
    public String toString() {
        return "Body{" +
                "local_incident_id=" + local_incident_id +
                ", orgName='" + orgName + '\'' +
                ", socId=" + socId +
                ", socName='" + socName + '\'' +
                ", org_id=" + org_id +
                ", system_impact=" + system_impact +
                ", critical_level=" + critical_level +
                ", nation_critical_infrastructure='" + nation_critical_infrastructure + '\'' +
                ", severity='" + severity + '\'' +
                ", incident_name='" + incident_name + '\'' +
                ", resolution='" + resolution + '\'' +
                ", status='" + status + '\'' +
                ", attack_vector='" + attack_vector + '\'' +
                ", description='" + description + '\'' +
                ", attack_time=" + attack_time +
                ", detected_time=" + detected_time +
                ", isolated_time=" + isolated_time +
                ", closed_time=" + closed_time +
                ", reported_time=" + reported_time +
                ", indicator=" + indicator +
                '}';
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getSocId() {
        return socId;
    }

    public void setSocId(Integer socId) {
        this.socId = socId;
    }

    public String getSocName() {
        return socName;
    }

    public void setSocName(String socName) {
        this.socName = socName;
    }
}
  