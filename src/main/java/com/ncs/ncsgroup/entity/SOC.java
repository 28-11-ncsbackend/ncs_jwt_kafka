package com.ncs.ncsgroup.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "soc_table")
public class SOC {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "`soc-id`")
    @JsonProperty("soc-id")
    private int soc_id;

    @Column(name = "`soc-name`", unique = true)
    @NotBlank(message = "Tên không được để trống")
    @JsonProperty("soc-name")
    private String soc_name;

    @Column(name = "`restrict-interval`")
    @JsonProperty("restrict-interval")
    @NotNull(message = "Giới hạn khoảng thời gian không được để trống.")
    private int restrict_interval;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "soc_org_table", joinColumns = { @JoinColumn(name = "`soc-id`") }, inverseJoinColumns = {
            @JoinColumn(name = "`org-id`") })
    private Set<ORG> orgs = new HashSet<>();

    @Column(name = "token", unique = true)
    private String token;

    public SOC() {

    }

    public int getSoc_id() {
        return soc_id;
    }

    public void setSoc_id(int soc_id) {
        this.soc_id = soc_id;
    }

    public String getSoc_name() {
        return soc_name;
    }

    public void setSoc_name(String soc_name) {
        this.soc_name = soc_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getRestrict_interval() {
        return restrict_interval;
    }

    public void setRestrict_interval(int restrict_interval) {
        this.restrict_interval = restrict_interval;
    }

    public Set<ORG> getOrgs() {
        return orgs;
    }

    public void setOrgs(Set<ORG> orgs) {
        this.orgs = orgs;
    }

}
