package com.ncs.ncsgroup.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "orgnization_table")
public class ORG {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "`org-id`")
	@JsonProperty("org-id")
	private int org_id;

	@Column(name = "`org-name`", unique = true)
	@NotBlank(message = "Tên tổ chức không được để trống.")
	@JsonProperty("org-name")
	private String org_name;

	
	public ORG() {

	}

	public int getOrg_id() {
		return org_id;
	}

	public void setOrg_id(int org_id) {
		this.org_id = org_id;
	}

	public String getOrg_name() {
		return org_name;
	}

	public void setOrg_name(String org_name) {
		this.org_name = org_name;
	}

	
}
