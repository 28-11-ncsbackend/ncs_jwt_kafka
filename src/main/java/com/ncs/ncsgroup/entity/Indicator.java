package com.ncs.ncsgroup.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Indicator {

    private String type;

    private String value;

    @JsonProperty("is-service-account")
    private String isServiceAccount;

    @JsonProperty("can-escalate-privs")
    private String canEscaltePrivs;

    private String key;
    
    @JsonProperty("is-privileged")
    private String isPrivileged;

    @JsonProperty("command-line")
    private String commandLine;

    private String hash;

    private String service;
    
    @JsonProperty("account-login")
    private String accountLogin;

    @JsonProperty("ip-v4")
    private String ipv4Addr;

    @JsonProperty("ip-v6")
    private String ipv6Addr;

    private String name;

    private String cwd;
    
    @JsonProperty("email-address")
    private String emailAdress;

    private String subject;
    
    private String message;

    @JsonProperty("external-id")
    private String external_id;

    private String version;

    private String vendor;

    private String cpe;

    @JsonProperty("file-name")
    private String fileName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getCpe() {
        return cpe;
    }

    public void setCpe(String cpe) {
        this.cpe = cpe;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getIpv4Addr() {
        return ipv4Addr;
    }

    public void setIpv4Addr(String ipv4Addr) {
        this.ipv4Addr = ipv4Addr;
    }

    public String getIpv6Addr() {
        return ipv6Addr;
    }

    public void setIpv6Addr(String ipv6Addr) {
        this.ipv6Addr = ipv6Addr;
    }

    public String getCwd() {
        return cwd;
    }

    public void setCwd(String cwd) {
        this.cwd = cwd;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getAccountLogin() {
        return accountLogin;
    }

    public void setAccountLogin(String accountLogin) {
        this.accountLogin = accountLogin;
    }

    public String getIsServiceAccount() {
        return isServiceAccount;
    }

    public void setIsServiceAccount(String isServiceAccount) {
        this.isServiceAccount = isServiceAccount;
    }

    public String getIsPrivileged() {
        return isPrivileged;
    }

    public void setIsPrivileged(String isPrivileged) {
        this.isPrivileged = isPrivileged;
    }

    public String getCanEscaltePrivs() {
        return canEscaltePrivs;
    }

    public void setCanEscaltePrivs(String canEscaltePrivs) {
        this.canEscaltePrivs = canEscaltePrivs;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
    
}
