package com.ncs.ncsgroup.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "ncs")
public class NcsConfig {
    private Kafka kafka = new Kafka();
    public static final class Kafka {

        private Ssl ssl = new Ssl();
        private Security security = new Security();

        public static final class Ssl {

            private String trust_store_location;

            private String trust_store_password;

            private String key_store_location;

            private String key_store_password;

            public String getTrust_store_location() {
                return trust_store_location;
            }

            public void setTrust_store_location(String trust_store_location) {
                this.trust_store_location = trust_store_location;
            }

            public String getTrust_store_password() {
                return trust_store_password;
            }

            public void setTrust_store_password(String trust_store_password) {
                this.trust_store_password = trust_store_password;
            }

            public String getKey_store_location() {
                return key_store_location;
            }

            public void setKey_store_location(String key_store_location) {
                this.key_store_location = key_store_location;
            }

            public String getKey_store_password() {
                return key_store_password;
            }

            public void setKey_store_password(String key_store_password) {
                this.key_store_password = key_store_password;
            }
        }

        public static final class Security {
            private String protocol;

            public String getProtocol() {
                return protocol;
            }

            public void setProtocol(String protocol) {
                this.protocol = protocol;
            }

        }
        private String topicname;
        private String kafkaserver;

        public String getTopicname() {
            return topicname;
        }

        public void setTopicname(String topicname) {
            this.topicname = topicname;
        }

        public String getKafkaserver() {
            return kafkaserver;
        }

        public void setKafkaserver(String kafkaserver) {
            this.kafkaserver = kafkaserver;
        }

        public Ssl getSsl() {
            return ssl;
        }

        public void setSsl(Ssl ssl) {
            this.ssl = ssl;
        }

        public Security getSecurity() {
            return security;
        }

        public void setSecurity(Security security) {
            this.security = security;
        }
    }

    private String debug;

    public String getDebug() {
        return debug;
    }

    private String adminkey;

    public String getAdminkey() {
        return adminkey;
    }

    public void setAdminkey(String adminkey) {
        this.adminkey = adminkey;
    }

    public void setDebug(String kafka) {
        this.debug = kafka;
    }

    public Kafka getKafka() {
        return kafka;
    }

    public void setKafka(Kafka kafka) {
        this.kafka = kafka;
    }

}
