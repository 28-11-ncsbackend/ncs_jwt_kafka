package com.ncs.ncsgroup.config;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConfiguration {

    @Autowired
    NcsConfig ncsConfig;

    @Bean
    public ProducerFactory<String, Object> producerFactory() {
        Map<String, Object> config = new HashMap<>();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,ncsConfig.getKafka().getKafkaserver() );
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        if(ncsConfig.getKafka().getSecurity().getProtocol().equals("SSL")){
            config.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, ncsConfig.getKafka().getSecurity().getProtocol());

            config.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,ncsConfig.getKafka().getSsl().getTrust_store_location());
            config.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, ncsConfig.getKafka().getSsl().getTrust_store_password());

            config.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, ncsConfig.getKafka().getSsl().getKey_store_location());
            config.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, ncsConfig.getKafka().getSsl().getKey_store_password());

            config.put(SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, " ");
        }
        return new DefaultKafkaProducerFactory(config);
    }

    @Bean
    public KafkaTemplate<String, Object> kafkaTemplate() {
        return new KafkaTemplate<String, Object>(producerFactory());
    }

    @Bean
    public ProducerFactory<String, String> producerFactoryString() {
        Map<String, Object> config = new HashMap<>();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ncsConfig.getKafka().getKafkaserver());
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory(config);
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplateString() {
        return new KafkaTemplate<String, String>(producerFactoryString());
    }

}